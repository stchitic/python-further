import pyglet
from math import sin, cos, pi, sqrt
twopi = 2*pi

from Display import Display

class PygletDisplay(Display):

    def __init__(self, model):
        super().__init__(model)
        self.window = pyglet.window.Window(600,400)
        self.fps_display = pyglet.clock.ClockDisplay()

        @self.window.event
        def on_draw():
            self.window.clear()
            self._model.draw(self)
            self.fps_display.draw()

    def go(self):
        pyglet.clock.schedule_interval(self.update, self._frame_rate)
        pyglet.app.run()

    @property
    def bounding_box(self):
        return (0, self.window.width, 0, self.window.height)

    def draw_circle(self, r, x,y, c):
        def circle_vertices():
            delta_angle = twopi / 20
            angle = 0
            while angle < twopi:
                yield x + r * cos(angle)
                yield y + r * sin(angle)
                angle += delta_angle

        pyglet.gl.glColor3f(*c.as_rgb_01())
        pyglet.graphics.draw(20, pyglet.gl.GL_LINE_LOOP,
                             ('v2f', tuple(circle_vertices())))
