from abc import ABC, abstractmethod

class Display(ABC):

    _frame_rate = 1 / 60

    def __init__(self, model):
        self._model = model

    def add(self, p):
        self._particles.append(p)

    def update(self, dt):
        self._model.update(dt, self)

    def draw(self):
        self._model.draw(self)

    @property
    @abstractmethod
    def bounding_box(self): pass

    @abstractmethod
    def go(self): pass

    @abstractmethod
    def draw_circle(self, r, x,y): pass

    # @abstractmethod
    # def draw_square(self, r, x,y): pass

    # @abstractmethod
    # def draw_whatever(self, r, x,y): pass

    # @abstractmethod
    # def draw_etc(self, r, x,y): pass
