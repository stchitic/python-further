class Colour:

    @classmethod
    def from_rgb_01(cls, r,g,b):
        c = cls()
        c._data = r,g,b
        return c

    def as_rgb_01(self):
        return self._data

    f_to_n = {d:n for n,d in enumerate('0123456789abcdef')}
    n_to_f = {n:d for d,n in f_to_n.items()}

    def as_rgb_f(self):
        return ''.join(self.n_to_f[int(v*15)] for v in self._data)

    @classmethod
    def from_rgb_f(cls, rgb):
        c = cls()
        c._data = tuple(cls.f_to_n[d] / 15.0 for d in rgb.lower())
        return c

    def __str__(self):
        d = self._data
        return f'<Colour: r={d[0]} g={d[1]} b={d[2]}'
    __repr__ = __str__

    def __hash__(self):        return hash(self._data)
    def __eq__  (self, other): return self._data == other._data

Color = Colour

Colour.BLACK   = Colour.from_rgb_01(0,0,0)
Colour.WHITE   = Colour.from_rgb_01(1,1,1)
Colour.RED     = Colour.from_rgb_01(1,0,0)
Colour.GREEN   = Colour.from_rgb_01(0,1,0)
Colour.BLUE    = Colour.from_rgb_01(0,0,1)
Colour.YELLOW  = Colour.from_rgb_01(1,1,0)
Colour.CYAN    = Colour.from_rgb_01(0,1,1)
Colour.MAGENTA = Colour.from_rgb_01(1,0,1)
